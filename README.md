# Final Yarışması Puanlama Algoritması

## Yarışma Puanlama Formülü

Herkese merhaba,

Bugün final yarışması puanlama algoritmasını açıklıyoruz. Temel olarak puanlama algoritması ön eleme yarışmasındakinden çok farklı değil. 
Detayları aşağıda bulabilirsiniz, ancak formüle geçmeden dikkat edilmesi gereken çok önemli bir nokta var:

**Final yarışması sonunda algoritmanız 3 kere çalıştırılacak ve bu 3 denemede aldığınız en iyi skor final yarışması skorunuz olacaktır.**

**Algoritmanız her hangi bir denemede diskalifiye olsa dahi, diskalifiye olmayan tekrarlar arasındaki en iyi skorunuz geçerli olacaktır. Ancak algoritmanızın resetlenebilir olduğuna dikkat edin. Sonsuz döngüler yaratmayın**

Yine de algoritmanızın stabil olduğuna mutlaka dikkat edin.
Birden fazla kere resetleyerek, pistten çıkıp çıkmadığınızı test edin. Eğer pistten çıktığınız oluyorsa, mutlaka algoritmanızı daha stabil hale getirin. 
Her ne kadar algoritmanız 3 kere denenecek olsa da, bir hakkınızın out of track ile sonuçlanmasını istemezsiniz.

Aşağıda Yarışma Puanı Hesaplama Formülü'nü paylaşıyoruz.

<br> </br>

<img src="Images/Final_Puanlama_Formulu.png" width="600">

<br> </br>


Puanlama algoritmasında ön eleme yarışmasındaki Sum Area metriğinin 0.5 m2 sınır koşulunu kaldırdık. Bu sınırın üstünde ve altında kalan yarışlara aynı katsayı uygulanacaktır. 

Not: Yarından itibaren RIDE Editör'deki metrikler bölümünde **Score** metriği eklenecektir. Bu metrikler üzerinden algoritmalarınızı optimize edebilirsiniz.

--------------------------------------

Aşağıda Final Yarışması ile ilgil takvimi bulabilirsiniz:

**Final Yarışması Başlangıç Tarihi: 16 Mayıs Saat 12:00**

**Final Yarışması Süresi: 12 Saat**

**Sonuçların Açıklanması: 17 Mayıs Saat 16:00 Canlı Yayın**

----------------------------------------------------

Aşağıda ilgili metrikler ile ilgili detayları bulabilirsiniz, temel olarak ön eleme yarışmasındaki metrik açıklamalarının aynısı. 
Yine de bir karışıklık yaratmaması adına, dokümana koymaya uygun gördük.

Öncelikle RIDE Editör’deki metrikleri kısaca açıklamakta fayda var. 
Yarışma metrikleriniz ile ilgili RIDE Editör'ünün alt kısmında lacivert bir bölüm var. 
Bu bölümde yarışma göstergeleri ve yarışma metrikleri gösteriliyor. 

<br> </br>

<img src="Images/Hazirlik_Parkuru.png" width="600">

<br> </br>

Öncelikle yarışma göstergelerini inceleyelim:

# Yarışma Göstergeleri

## Disqualification Reason:

*  **Null:** Diskalifiye edilmedi

*  **Incorrect Start:** Robot başlaması gereken noktadan farklı bir noktada başladı

*  **Out of Track:** 1m x 1m’lik karoların dışına çıkıldı

*  **Out of Sequence:** En az 1 parkur karosunun içinden geçilmeden atlandı

*  **Out of Time:** Parkur verilen azami süre içerisinde tamamlanmadı. Bu azami süre deneme ortamında **200**, ön eleme ortamında **100** sn'dir.

Gerçek yarışmada bu durumlardan herhangi birisi gerçekleşirse yarışmadan elenirsiniz. 
Yarışma sonunda algoritmanızı 3 kere çalıştırıp en iyi skorunuzu alacağız ancak yine de algoritmanızı çok defa resetleyerek her seferinde çalıştığından emin olun, pistten çıkmalar varsa algoritmanızı mutlaka daha stabil hale getirin.
## **race started:**(True/False)

Simulatör start aldığında veya resetlendiğinde ve robot herhangi bir harekette bulunmadığı sürece race started: False olarak kalır. Robot ilk hareketini gerçekleştirdiği anda race started: True olur.

## **race finished:** (True/False)
Yarışmacı yukarıdaki elenme durumlarını gerçekleştirmeden parkuru tamamlarsa, race finished: True olur.
Parkurun ilk turu dikkate alınmaktadır. race finished : True olduğu anda metrikler durmaktadır, bu da ilk turun sonunda gerçekleşmektedir. 

# Yarışma Metrikleri

## Sum Area (m2): 

Yarışma kurallarında da açıkladığımız üzere sum area, robotun çizgiden ne kadar saptığını gösteren bir metriktir. 
Çizgi boyunca çizgiden sapılan alan sürekli olarak puanlama algoritması tarafından hesaplanmaktadır. 
Sum area ne kadar düşükse sıralamada o kadar yükseleceksiniz. 

<br> </br>

<img src="Images/Yarisma_Kurallari_1.png" width="600">

<br> </br>

## Time (s):

Yarışı ne kadar sürede bitirdiğinizi gösteren metriktir. Yani göstergeler üzerinden konuşacak olursak race started: True ile race finished: True arasındaki geçen zamandır. Haliyle bu süre ne kadar kısa olursa puanınız o kadar yüksek olacaktır. 
Sum Distance (m): Yarış süresince katettiğiniz mesafeyi gösteren bir metriktir. 

Eğer yarış sonucunda aldığınız Sum Distance değeri Toplam Pist Uzunluğu’ndan kısa olursa, puanlama algoritması bitirme sürenize ceza olarak 
kalan mesafe / ortalama hızınız kadar süre ekler ve "Corrected Time" diye yeni bir metrik hesaplar.

<br> </br>

<img src="Images/Final_Corrected_Time_Formul.png" width="600">

<br> </br>

Bu metrik skor hesaplama formülünde kullanılmaktadır.

Aşağıda bir örnek ile açıklayalım:

<br> </br>

<img src="Images/Corrected_Time_F.png" width="800">

<br> </br>

Örnekte görüldüğü üzere, yarışmacı pisti 45 saniyede bitirmesine karşın, daha kısa bir yoldan gittiği için yarışı bitirme süresine bir ceza süresi eklenmiştir ve bitirme süresi 49.493 saniye olmuştur.

----------------------------------------------

Evet metrik açıklamalarını da yaptıktan sonra, sizlere keyifli ve başarılı bir yarışma diliyoruz.

Görüşmek üzere!

Riders Ekibi



